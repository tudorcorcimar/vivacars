var banner = {
    init: function(selector){
        var conf = {
            that        : this,
            arrowLeft   : $( selector + ' .arrow.left'),
            arrowRight  : $( selector + ' .arrow.right'),
            posts       : $( selector + ' .banner-post'),
            images      : $( selector + ' .images .image'),
            title       : $( selector + ' .title'),
            shortDescr  : $( selector + ' .short-description'),
            postDescr   : $( selector + ' .post-description')
        };
        var currentPostId = null;
        var nextPost = function(){
            if(!currentPostId){
                currentPostId = 1;
            }
            $('#post-' + currentPostId).addClass('hidden-post');
            if(currentPostId === $(conf.posts).length){
                currentPostId = 1;
            }else{
                currentPostId = currentPostId + 1;
            }
            $('#post-' + currentPostId).removeClass('hidden-post');
        };
        var prevPost = function(){
            if(!currentPostId){
                currentPostId = 1;
            }
            $('#post-' + currentPostId).addClass('hidden-post');
            if(currentPostId === 1){
                currentPostId = $(conf.posts).length;
            }else{
                currentPostId = currentPostId - 1;
            }
            $('#post-' + currentPostId).removeClass('hidden-post');
        };
        $(conf.arrowRight).on('click', function(){
            nextPost();
        });
        $(conf.arrowLeft).on('click', function(){
            prevPost();
        });
    }
};
banner.init('.general-banner');