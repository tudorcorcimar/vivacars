<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use App\Utils\Captha;

/**
 * Get captcha
 */
Route::get('/captcha', function(){
    return Captha::getCaptcha();
});

/*
* Front End
*/

Route::group(array('namespace' => '\Frontend'), function() {
    
    Route::group(array('namespace' => '\Home'), function() {
        Route::any('/', ['uses' => 'HomeController@showHome']);
        Route::any('/home', ['uses' => 'HomeController@showHome']);
    });
    
    Route::group(array('namespace' => '\Macchine'), function() {
        Route::any('/macchine', ['uses' => 'MacchineController@showAllMacchine']);
        Route::any('/macchine/{id}/description', ['uses' => 'MacchineController@showMacchine']);
    });
    
});