<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

abstract class BaseController extends Controller
{
    
    /**
     * @var array Variable to store all view data.
     */
    protected $viewbag = array();
    
    /**
     * @const string Company meta Seo tags
     * 
     * COMPANY_META_TITLE ---------- <title>title of page</title> important for SEO
     * COMPANY_META_DESCRIPTION ---- <meta name="description" content="some page description"> max 160 symbols 
     * COMPANY_META_DEFAULT_ROBOTS - <meta name="robots" content="index,nofollow">
     *                               content = all | none | directives
     *                               all = «ALL»
     *                               none = «NONE»
     *                               directives = directive [«,» directives]
     *                               directive = index | follow
     *                               index = «INDEX» | «NOINDEX»
     *                               follow = «FOLLOW» | «NOFOLLOW»
     * COMPANY_META_CONTENT_TYPE --- <meta http-equiv="Content-Type" content="Type=text/html; charset=utf-8">
     *                               short version of current metateg <meta charset="utf-8" />
     *                               this is not required for SEO development and it's better to use short version
     *                               it need to set befor <title>
     * COMPANY_META_KEYWORDS ------- <meta name="keywords" content="keywords" /> this tag will not influence to 
     *                               ranking of web resource most seach sistems will ignore it
     * COMPANY_META_LANGUAGE ------- <meta http-equiv="content-language" content="fr" />
     *                               today most used is short version <html lang="en">
     *                               and it can be used for other tags as <p lang="es">Me gusta..
     * COMPANY_META_TRANSLATION ---- <meta name="google" content="notranslate" /> just if dont wont to translate our page
     * COMPANY_META_REFRASH -------- <meta http-equiv="refresh" content="30"> if we wot to refrash browser page after 30 seconds
     *                               <meta http-equiv="refresh" content="30;URL='google.com'"> refrash with redirect
     * 
     * note: most important tags is DESCRIPTION, ROBOTS, TITLE, H1, H2, H3
     * 
     */
    const COMPANY_META_TITLE          = 'Viva Cars S.r.l.';
    const COMPANY_META_DESCRIPTION    = 'Viva Cars S.r.l.: Vendita di macchine utensili nuove, usate e revisionate.';
    const COMPANY_META_DEFAULT_ROBOTS = 'index, follow';
    const COMPANY_META_CONTENT_TYPE   = 'Type=text/html; charset=utf-8';
    const COMPANY_META_KEYWORDS       = 'Viva Cars, VivaCars, Viva, Cars, utensili, usate';
    const COMPANY_META_LANGUAGE       = 'it';
    const COMPANY_META_TRANSLATION    = 'notranslate';
    const COMPANY_META_REFRASH_TIME   = '3000'; // seconds
    const COMPANY_META_REFRASH_URL    = ''; // url
    
    /**
     * @const string Company general info
     */
    const COMPANY_NAME    = 'Viva Cars S.r.l.';
    const COMPANY_URL     = 'www.vivacars.com';
    const COMPANY_EMAIL   = 'info@vivacars.com';
    const COMPANY_PHONE   = '+39 3355320426';
    const COMPANY_SKYPE   = 'vivacars';
    const COMPANY_ADDRESS = 'Viva Cars S.r.l., Via Mazzini,13 Milano 20121 Italia';
    
    /**
     * @const string Social networks url
     */
    const COMPANY_SOCIAL_FACEBOOK_URL = 'http:\\\facebook.com';
    const COMPANY_SOCIAL_TWITTER_URL  = 'http:\\\twiter.com';
    const COMPANY_SOCIAL_LINKEDIN_URL = 'http:\\\linkedin.com';
    
    /**
     * @const string Macchine
     */
    const MACCHINE_PATH = 'resources/macchine';
    const MACCHINE_IMAGES = 'img';
    const MACCHINE_INFO = 'info.json';
    
    /**
     * Get general view data
     */
    private function getViewbag() {
        
        // Viewbag collector
        $bag                                   = [];
        
        // Application array`
        $bag['app']                            = [];
        
        // Collect meta info
        $bag['app']['meta']                    = [];
        $bag['app']['meta']['title']           = self::COMPANY_META_TITLE;
        $bag['app']['meta']['description']     = self::COMPANY_META_DESCRIPTION;
        $bag['app']['meta']['robots']          = self::COMPANY_META_DEFAULT_ROBOTS;
        $bag['app']['meta']['contenttype']     = self::COMPANY_META_CONTENT_TYPE;
        $bag['app']['meta']['keywords']        = self::COMPANY_META_KEYWORDS;
        $bag['app']['meta']['language']        = self::COMPANY_META_LANGUAGE;
        $bag['app']['meta']['translation']     = self::COMPANY_META_TRANSLATION;
        // Meta refrash
        $bag['app']['meta']['refrash']         = [];
        $bag['app']['meta']['refrash']['time'] = self::COMPANY_META_REFRASH_TIME;
        $bag['app']['meta']['refrash']['url']  = self::COMPANY_META_REFRASH_URL;
        
        // Collect company base info
        $bag['app']['name']                    = self::COMPANY_NAME;
        $bag['app']['url']                     = self::COMPANY_URL;
        $bag['app']['email']                   = self::COMPANY_EMAIL;
        $bag['app']['phone']                   = self::COMPANY_PHONE;
        $bag['app']['skype']                   = self::COMPANY_SKYPE;
        $bag['app']['address']                 = self::COMPANY_ADDRESS;
        
        // Collect social info
        $bag['app']['social']['facebook']['url']      = self::COMPANY_SOCIAL_FACEBOOK_URL;
        $bag['app']['social']['twitter']['url']       = self::COMPANY_SOCIAL_TWITTER_URL;
        $bag['app']['social']['linkedin']['url']      = self::COMPANY_SOCIAL_LINKEDIN_URL;
        
        return $bag;
    }
    
    /**
     * @return array list of macchine
     */
    public function getMacchines(){
        
        // Macchine collector
        $macchines = [];
        $basePath = str_replace('public', env('PATH_PUBLIC'),public_path()."/".self::MACCHINE_PATH);
        //$basePath = public_path()."/".self::MACCHINE_PATH;
        $dir = array_diff(scandir($basePath), array('..', '.'));
        foreach($dir as $folder){
            $macchineDir = $basePath."/".$folder;
            $fileInfo = $macchineDir."/".self::MACCHINE_INFO;
            $macchineJson = file_get_contents($fileInfo);
            $macchine = json_decode($macchineJson, true);
            $macchine['imagespath'] = url('')."/".self::MACCHINE_PATH."/".$folder."/".self::MACCHINE_IMAGES;
            $macchines[] = $macchine;
        }
        return $macchines;
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->viewbag = self::getViewbag();
    }
}
