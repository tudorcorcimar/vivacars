<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;
use App\Utils\Captha;

class HomeController extends BaseController
{
    
    /**
     * Constructor.
     */
    public function __construct() {
        parent::__construct();
        $macchines = $this->getMacchines();
        $this->viewbag['macchines'] = $macchines;
    }
    
    public function showHome(){
        $this->viewbag['menu'] = 'home';
        return View::make("frontend.home.home", $this->viewbag);
    }
    
}
